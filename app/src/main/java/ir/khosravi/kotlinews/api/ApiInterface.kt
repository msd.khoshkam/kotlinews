package ir.khosravi.kotlinews.api

import ir.khosravi.kotlinews.model.ResponseModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

interface ApiInterface {

    @GET
    fun getNews(@Url url: String): Call<ResponseModel>
}