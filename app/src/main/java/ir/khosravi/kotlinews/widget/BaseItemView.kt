package ir.khosravi.kotlinews.widget

import android.annotation.SuppressLint
import android.content.Context
import androidx.annotation.LayoutRes
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import ir.khosravi.kotlinews.utils.App

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

/**
 * This class is an abstract To get the 'Layout Resource ID' of its subclasses and update them
 */

@SuppressLint("NewApi")
abstract class BaseItemView(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyle, defStyleRes) {

    constructor() : this(App.context, null, 0)
    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    init {
        LayoutInflater.from(context).inflate(this.getLayoutResource(), this)
    }

    @LayoutRes
    abstract fun getLayoutResource(): Int

    open fun updateView() {}

}