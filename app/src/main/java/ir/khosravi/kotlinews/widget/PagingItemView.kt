package ir.khosravi.kotlinews.widget

import android.annotation.SuppressLint
import android.graphics.Color
import ir.khosravi.kotlinews.R
import kotlinx.android.synthetic.main.view_item_paging.view.*

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

/**
 * This is a ViewClass that is child of {@link #BaseItemView} to the set CategoryName on the View.
 */

@SuppressLint("ViewConstructor")
class PagingItemView : BaseItemView() {

    var pageNumber: Int? = null
        set(value) {
            field = value
            updateView()
        }

    override fun getLayoutResource(): Int = R.layout.view_item_paging

    override fun updateView() {
        this.tv_page_number.text = pageNumber?.toString()

        if (this.tv_page_number.isEnabled)
            this.tv_page_number.setTextColor(Color.WHITE)
        else
            this.tv_page_number.setTextColor(rootView.context.resources.getColor(R.color.colorPrimaryDark))

    }
}

