package ir.khosravi.kotlinews.widget

import android.annotation.SuppressLint
import ir.khosravi.kotlinews.R
import kotlinx.android.synthetic.main.view_item_news.view.*

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

/**
 * This is a ViewClass that is child of {@link #BaseItemView} to the set CategoryName on the View.
 */

@SuppressLint("ViewConstructor")
class CategoryItemView : BaseItemView() {

    var categoryName: String? = null
        set(value) {
            field = value
            updateView()
        }

    override fun getLayoutResource(): Int = R.layout.view_item_category

    override fun updateView() {
        super.updateView()
        categoryName.let {
            this.title.text = categoryName
        }
    }
}

