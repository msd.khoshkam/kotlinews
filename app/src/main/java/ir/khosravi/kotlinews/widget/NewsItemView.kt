package ir.khosravi.kotlinews.widget

import android.annotation.SuppressLint
import ir.khosravi.kotlinews.R
import ir.khosravi.kotlinews.model.NewsModel
import ir.khosravi.kotlinews.utils.DateUtils
import ir.khosravi.kotlinews.utils.ImageUtils
import kotlinx.android.synthetic.main.view_item_news.view.*

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

/**
 * This is a ViewClass that is child of {@link #BaseItemView} to the set {@link NewsModel} DataClass on the Views.
 *
 */


@SuppressLint("ViewConstructor")
class NewsItemView : BaseItemView() {

    var newsModel: NewsModel? = null
        set(value) {
            field = value
            updateView()
        }

    override fun getLayoutResource(): Int = R.layout.view_item_news

    override fun updateView() {
        super.updateView()
        newsModel.let {
            newsModel?.urlToImage?.let { urlToImage ->
                ImageUtils.setImage(urlToImage, iv_image)
            }
            this.title.text = newsModel?.title
            this.tv_description.text = newsModel?.description
            this.tv_date.text = DateUtils.getItemsDate(newsModel?.publishedAt!!)
        }
    }
}

