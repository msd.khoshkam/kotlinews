package ir.khosravi.kotlinews.model

data class ErrorModel(
    val status: String? = "",
    val code: String? = null,
    val message: String? = null
)