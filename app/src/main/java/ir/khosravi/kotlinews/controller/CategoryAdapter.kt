package ir.khosravi.kotlinews.controller

import ir.khosravi.kotlinews.widget.CategoryItemView

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */


class CategoryAdapter(private val categoryList: ArrayList<String>) :
    BaseAdapter<String, CategoryItemView>(categoryList, CategoryItemView::class.java) {

    override fun onBindViewHolder(viewHolder: BaseViewHolder, position: Int) {
        super.onBindViewHolder(viewHolder, position)
        val rootView = viewHolder.itemView
        val categoryName = categoryList[position]
        val categoryItemView = rootView as CategoryItemView

        categoryItemView.categoryName = categoryName
    }
}