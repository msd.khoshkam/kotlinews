package ir.khosravi.kotlinews.controller

import ir.khosravi.kotlinews.widget.PagingItemView
import kotlinx.android.synthetic.main.view_item_paging.view.*

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

class PagingAdapter(
    private val pageNumberList: ArrayList<Int>,
    var selectedPage: Int
) : BaseAdapter<Int, PagingItemView>(pageNumberList, PagingItemView::class.java) {

    override fun getItemCount(): Int = pageNumberList.size

    override fun onBindViewHolder(viewHolder: BaseViewHolder, position: Int) {
        super.onBindViewHolder(viewHolder, position)
        val pagingItemView = viewHolder.itemView as PagingItemView

        pagingItemView.isEnabled = selectedPage != pageNumberList[position]
        pagingItemView.tv_page_number.isEnabled = selectedPage != pageNumberList[position]

        pagingItemView.pageNumber = pageNumberList[position]
    }
}