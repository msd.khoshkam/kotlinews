package ir.khosravi.kotlinews.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

class DateUtils {
    companion object {
        private const val inputDateFormat: String =
            "yyyy-MM-dd'T'HH:mm:ss'Z'"  // 2019-09-28T09:05:23Z

        private const val detailDateFormat: String =
            "HH:mm - dd MMM yyyy"       // 22:00 - 26 Jul 2017

        private const val itemsDateFormat: String =
            "dd MMM yyyy"               // "26 Jul 2017"


        fun getDetailDate(time: String): String = getDate(time, detailDateFormat)

        fun getItemsDate(time: String): String = getDate(time, itemsDateFormat)

        private fun getDate(time: String, outputFormat: String): String {
            val inputDateFormat = SimpleDateFormat(inputDateFormat, Locale.US)
            val outputDateFormat = SimpleDateFormat(outputFormat, Locale.US)
            var result: String? = null

            try {
                val date = inputDateFormat.parse(time)
                result = outputDateFormat.format(date)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return result!!
        }
    }
}