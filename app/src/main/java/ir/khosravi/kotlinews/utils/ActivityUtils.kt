package ir.khosravi.kotlinews.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import ir.khosravi.kotlinews.R
import ir.khosravi.kotlinews.model.NewsModel

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

class ActivityUtils {
    companion object {
        const val NEWS_LIST_SPAN_COUNT: Int = 2
        const val MAXIMUM_NEWS_LIST_SIZE: Int = 20
        const val AN_OBJECT_PARCEL_NAME: String = "ar_an_object"

        fun shareNewsDetail(context: Context, newsModel: NewsModel) {
            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, newsModel.title)
            sharingIntent.putExtra(Intent.EXTRA_TEXT, newsModel.url)
            sharingIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(
                Intent.createChooser(
                    sharingIntent,
                    context.resources.getString(R.string.share_using)
                )
            )
        }
    }
}