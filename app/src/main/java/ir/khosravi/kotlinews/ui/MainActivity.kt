package ir.khosravi.kotlinews.ui

import android.content.Intent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import ir.khosravi.kotlinews.R
import ir.khosravi.kotlinews.databinding.ActivityMainBinding
import ir.khosravi.kotlinews.model.NewsModel
import ir.khosravi.kotlinews.utils.ActivityUtils
import ir.khosravi.kotlinews.viewModel.MainViewModel
import kotlinx.android.synthetic.main.main_bottom_sheet.*
import kotlinx.android.synthetic.main.main_content.*

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

class MainActivity : BaseActivity() {

    private var mBottomSheetBehavior: BottomSheetBehavior<*>? = null
    private val mSelectedNewsModelObserver = Observer<NewsModel> { model ->
        val intent = Intent(this@MainActivity, NewsDetailActivity::class.java)
        intent.putExtra(ActivityUtils.AN_OBJECT_PARCEL_NAME, model as NewsModel)
        startActivity(Intent(intent))
    }
    private val mBottomSheetStateObserver = Observer<Int> { state ->
        mBottomSheetBehavior?.state = state
    }

    override fun getLayoutResource(): Int = R.layout.activity_main

    override fun doOtherTask() {
        mBottomSheetBehavior = BottomSheetBehavior.from(main_bottom_sheet)

        rv_category.layoutManager = LinearLayoutManager(this)
        rv_news.layoutManager = GridLayoutManager(this, ActivityUtils.NEWS_LIST_SPAN_COUNT)
        rv_paging.layoutManager = LinearLayoutManager(
            this, LinearLayoutManager.HORIZONTAL, false
        )

        val vm: MainViewModel by lazy { ViewModelProviders.of(this).get(MainViewModel::class.java) }


        val mainViewModel: MainViewModel = getViewModel()
        mainViewModel.getSelectedNewsModel().observe(this, mSelectedNewsModelObserver)
        mainViewModel.getBottomSheetState().observe(this, mBottomSheetStateObserver)

        val mainActivityBinding = mBinding as ActivityMainBinding
        mainActivityBinding.mainToolbar.mainViewModel = mainViewModel
        mainActivityBinding.mainContent.mainViewModel = mainViewModel
        mainActivityBinding.mainBottomSheet.mainViewModel = mainViewModel

        mainViewModel.getResponseModel(mainViewModel.getDefaultCategory().value!!, 1)
    }

    override fun getViewModel(): MainViewModel =
        ViewModelProviders.of(this).get(MainViewModel::class.java)

}

