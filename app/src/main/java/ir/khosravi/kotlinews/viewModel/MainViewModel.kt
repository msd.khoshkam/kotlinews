package ir.khosravi.kotlinews.viewModel

import android.view.View
import androidx.annotation.IntDef
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ir.khosravi.kotlinews.R
import ir.khosravi.kotlinews.api.NetworkUtils
import ir.khosravi.kotlinews.api.RequestsRepo
import ir.khosravi.kotlinews.api.ResponseListener
import ir.khosravi.kotlinews.controller.CategoryAdapter
import ir.khosravi.kotlinews.controller.NewsAdapter
import ir.khosravi.kotlinews.controller.PagingAdapter
import ir.khosravi.kotlinews.interfaces.OnItemClickListener
import ir.khosravi.kotlinews.model.ErrorModel
import ir.khosravi.kotlinews.model.NewsModel
import ir.khosravi.kotlinews.model.ResponseModel
import ir.khosravi.kotlinews.utils.ActivityUtils


/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

open class MainViewModel : ViewModel() {

    private var mLastPageNumber: Int = 1
    private val newsListMaxSize = ActivityUtils.MAXIMUM_NEWS_LIST_SIZE
    private lateinit var mPagingAdapter: PagingAdapter
    private lateinit var mResponseModel: MutableLiveData<ResponseModel>
    private var mCategoryListAdapter: MutableLiveData<CategoryAdapter> =
        MutableLiveData<CategoryAdapter>()
    private var mNewsListAdapter: MutableLiveData<NewsAdapter> =
        MutableLiveData<NewsAdapter>()
    private var mPagingListAdapter: MutableLiveData<PagingAdapter> =
        MutableLiveData<PagingAdapter>()
    private var mSelectedNewsModel: MutableLiveData<NewsModel> =
        MutableLiveData<NewsModel>()
    private var mDefaultCategory: MutableLiveData<String> =
        MutableLiveData<String>()
    private var mBottomSheetState: MutableLiveData<Int> =
        MutableLiveData<Int>()
    private val mNewsItemClickListener = object : OnItemClickListener {
        override fun onItemClick(model: Any?) {
            if (model != null && model is NewsModel) setSelectedNewsModel(model)
        }
    }
    private val mPagingItemClickListener = object : OnItemClickListener {
        override fun onItemClick(model: Any?) {
            if (model != null && model is Int) {
                getResponseModel(getDefaultCategory().value!!, model)
                mPagingAdapter.selectedPage = model
                mLastPageNumber = model

                mPagingAdapter.notifyDataSetChanged()
            }
        }
    }
    private val mCategoryItemClickListener = object : OnItemClickListener {
        override fun onItemClick(model: Any?) {
            if (model != null && model is String) {
                setDefaultCategory(model)
                mLastPageNumber = 1
                getResponseModel(model, mLastPageNumber)
            }
            setBottomSheetState(BottomSheetState.HIDE)
        }
    }

    init {
        setBottomSheetState(BottomSheetState.HIDE)
        setDefaultCategory(NetworkUtils.Parameters.getCategoryNames()[0])
        createCategoryListAdapter()
    }

    fun onClick(view: View) {
        if(view.id == R.id.btn_filter)
        when (mBottomSheetState.value) {
            BottomSheetState.HIDE -> setBottomSheetState(BottomSheetState.EXPAND)
            BottomSheetState.EXPAND -> setBottomSheetState(BottomSheetState.HIDE)
        }
    }

    fun getBottomSheetState(): LiveData<Int> = mBottomSheetState

    fun getResponseModel(category: String?, page: Int): LiveData<ResponseModel> {
        mResponseModel = MutableLiveData<ResponseModel>()
        loadNewsList(category, page)
        return mResponseModel
    }

    fun getDefaultCategory(): LiveData<String> = mDefaultCategory

    fun getSelectedNewsModel(): LiveData<NewsModel> = mSelectedNewsModel

    fun getCategoryListAdapter(): LiveData<CategoryAdapter> = mCategoryListAdapter

    fun getNewsListAdapter(): LiveData<NewsAdapter> = mNewsListAdapter

    fun getPagingListAdapter(): LiveData<PagingAdapter> = mPagingListAdapter

    private fun loadNewsList(category: String?, page: Int) {
        RequestsRepo.getNews(category, page, object : ResponseListener {
            override fun OnResponse(responseModel: ResponseModel) {
                if (::mResponseModel.isInitialized) {
                    mResponseModel.postValue(responseModel)
                    createNewsListAdapter(responseModel.newsList)

                    if (responseModel.totalResults > newsListMaxSize && mLastPageNumber == 1) {
                        setPagingListView(getAvailablePageList(responseModel.totalResults))
                    }
                }
            }

            override fun OnError(errorModel: ErrorModel) {
                // TODO : Handle Error
            }
        })
    }

    private fun setDefaultCategory(categoryName: String) {
        mDefaultCategory.value = categoryName
    }

    private fun setSelectedNewsModel(newsModel: NewsModel) {
        mSelectedNewsModel.postValue(newsModel)
    }

    private fun createNewsListAdapter(newsList: ArrayList<NewsModel>) {
        val newsListAdapter = NewsAdapter(newsList)
        newsListAdapter.itemClickListener = mNewsItemClickListener
        mNewsListAdapter.value = newsListAdapter
    }

    private fun createCategoryListAdapter() {
        val categoryAdapter = CategoryAdapter(NetworkUtils.Parameters.getCategoryNames())
        categoryAdapter.itemClickListener = mCategoryItemClickListener
        mCategoryListAdapter.postValue(categoryAdapter)
    }

    private fun getAvailablePageList(totalResults: Int): ArrayList<Int> {
        var count = totalResults / newsListMaxSize
        if (totalResults % newsListMaxSize < 0) count++

        val pageNumberList = ArrayList<Int>(count)
        for (i in 1..count + 1) pageNumberList.add(i)
        return pageNumberList
}

    private fun setPagingListView(pageNumberList: ArrayList<Int>) {
        mPagingAdapter = PagingAdapter(pageNumberList, 1)
        mPagingAdapter.itemClickListener = mPagingItemClickListener
        mPagingListAdapter.value = mPagingAdapter
    }

    private fun setBottomSheetState(@BottomSheetState.Companion.State state: Int) {
        mBottomSheetState.value = state
    }

    open class BottomSheetState {
        companion object {
            // The initial value is defined as the
            // 'BottomSheetBehavior.STATE_EXPANDED' and 'BottomSheetBehavior.STATE_HIDDEN'
            const val EXPAND = 3
            const val HIDE = 5

            @IntDef(EXPAND, HIDE)
            @Retention(AnnotationRetention.SOURCE)
            annotation class State
        }
    }

}