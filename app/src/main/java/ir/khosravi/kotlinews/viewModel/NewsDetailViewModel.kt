package ir.khosravi.kotlinews.viewModel

import android.view.View
import androidx.lifecycle.ViewModel
import ir.khosravi.kotlinews.R
import ir.khosravi.kotlinews.model.NewsModel
import ir.khosravi.kotlinews.utils.ActivityUtils
import ir.khosravi.kotlinews.utils.App.Companion.context

class NewsDetailViewModel : ViewModel() {

     lateinit var newsModel: NewsModel

    fun onClick(v: View) {
        if (v.id == R.id.btn_share && ::newsModel.isInitialized) {
            ActivityUtils.shareNewsDetail(context , newsModel)
        }
    }

}